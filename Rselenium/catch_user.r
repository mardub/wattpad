# This is a short script to catch one username.
# Inspired from: https://github.com/mardub1635/RSelenium_scraping_tutorial/blob/master/RSelenium_scraping_tutorial.md
# Before running this R script make sure you run the following command on your terminal:
# sudo docker run -d -p 4445:4444 selenium/standalone-firefox:2.53.0

library(RSelenium)

remDr <- remoteDriver(port = 4445L) # instantiate remote driver to connect to Selenium Server
remDr$open() # open web browser


url = "https://www.wattpad.com/585934315-k%C3%A4rlek-b%C3%B6rjar-alltid-med-br%C3%A5k-del-01"
remDr$navigate(url)
page_title <- unlist(remDr$getTitle())
cat("The title of the page is:\n", page_title)

#extract one username and write it to a file
user_html <- remDr$findElement(using = 'class', value = 'username')
user_text <- unlist(user_html$getElementText())
cat("This is a user name:", user_text)
#writeLines(user_text,"user_removeme.txt") #uncomment this line if you want to write the username to a .txt file

