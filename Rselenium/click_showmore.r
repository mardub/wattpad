# This is a short script to catch two usernames.
# Inspired from: https://github.com/mardub1635/RSelenium_scraping_tutorial/blob/master/RSelenium_scraping_tutorial.md
# Before running this R script make sure you run the following command on your terminal:
# sudo docker run -d -p 4445:4444 selenium/standalone-firefox:2.53.0

library(RSelenium)

remDr <- remoteDriver(port = 4445L) # instantiate remote driver to connect to Selenium Server
remDr$open() # open web browser


url = "https://www.wattpad.com/585934315-k%C3%A4rlek-b%C3%B6rjar-alltid-med-br%C3%A5k-del-01"
remDr$navigate(url)
MoreCommButton <- remDr$findElement("class name", "show-more")
MoreCommButton$clickElement()
remDr$screenshot(display = T)

MoreCommButton <- remDr$findElement("class name", "show-more")
MoreCommButton$clickElement()


#extract usernames and show it.
user_html <- remDr$findElements(using = 'class', value = 'username')
user_text <- unlist(lapply(user_html, function(x){x$getElementText()}))

cat("These are a user names:", user_text)
remDr$screenshot(display = T)
